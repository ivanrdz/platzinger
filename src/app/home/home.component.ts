import { Component, OnInit } from '@angular/core';
import { User } from './../interfaces/user';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  friends: User[];
  constructor() {
    let usuario1: User = {
      nick: 'Eduardo',
      age: 28,
      email: 'edo@gmail.com',
      friend: true,
      uid: 1
    };
    let usuario2: User = {
      nick: 'Ivan',
      age: 23,
      email: 'ivano@gmail.com',
      friend: true,
      uid: 1
    };
    let usuario3: User = {
      nick: 'Jose',
      age: 24,
      email: 'jose@gmail.com',
      friend: false,
      uid: 1
    };
    let usuario4: User = {
      nick: 'Ricardo',
      age: 25,
      email: 'ricardo@gmail.com',
      friend: false,
      uid: 1
    };
    let usuario5: User = {
      nick: 'Raymundo',
      age: 29,
      email: 'ray@gmail.com',
      friend: false,
      uid: 1
    };
    this.friends = [usuario1, usuario2, usuario3, usuario4, usuario5];
    console.log(this.friends);
  }

  ngOnInit() {
  }

}
